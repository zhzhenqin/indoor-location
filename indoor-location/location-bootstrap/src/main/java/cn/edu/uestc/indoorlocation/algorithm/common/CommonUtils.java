package cn.edu.uestc.indoorlocation.algorithm.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.edu.uestc.indoorlocation.dao.model.Rss;

public class CommonUtils {
	
	public static final int MIN_RSS_VAL = -100;
	public static List<Rss> rssFilter(List<Rss> rssis) {
		
		List<Rss> ret = new ArrayList<Rss>();
		for (Rss rss : rssis) {
			if (Integer.compare(rss.RSS(), MIN_RSS_VAL) >= 0) {
				ret.add(rss);
			}
		}
		return ret;
	}
	
	/**
	 * 首先得到rss1和rss2的MAC交集，
	 * 然后返回rss2相应MAC的rss2的一个子集
	 * @param rss1
	 * @param rss2
	 * @return
	 */
	public static List<ArrayList<Rss>> macIntersection(List<Rss> rss1, List<Rss> rss2) {
		
		List<ArrayList<Rss>> result = new ArrayList<ArrayList<Rss>>();
		ArrayList<Rss> ret1 = new ArrayList<>();
		ArrayList<Rss> ret2 = new ArrayList<>();
		//先排序，提高查找命中率
//		Collections.sort(rss2);
		//假设rss2已排序
		for (Rss rss : rss1) {
			int len = rss2.size();
			boolean find = false;
			for (int i = 0; i < len; i++) {
				if (rss2.get(i).MAC().equals(rss.MAC())) {
					find = true;
					ret1.add(rss);
					ret2.add(rss2.get(i));
					rss2.remove(i);
					break;
				}
			}
		}
		result.add(ret1);
		result.add(ret2);
		return result;
	}
	
	public static void main(String[] args) {
		
	}
	
}