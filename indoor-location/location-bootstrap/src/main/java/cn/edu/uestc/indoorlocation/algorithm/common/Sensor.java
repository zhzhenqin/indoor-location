package cn.edu.uestc.indoorlocation.algorithm.common;

/**
 * 客户端传感器信息
 * @author vincent
 *
 */
public class Sensor {

	/**
	 * 航向角
	 */
	private final double azimuthAngle;
	/**
	 * 是否步进
	 */
	private final boolean isStep;
	/**
	 * 东西方向加速度
	 */
	private final double weAcceration;
	/**
	 * 南北方向加速度
	 */
	private final double nsAcceleration;
	/**
	 * 上下方向加速度
	 */
	private final double udAcceleration;
	
	public Sensor(double azi, boolean step, double we, double ns, double ud) {
		
		this.azimuthAngle = azi;
		this.isStep = step;
		this.weAcceration = we;
		this.nsAcceleration = ns;
		this.udAcceleration = ud;
	}
	
	public double azimuthAngle() {
		return this.azimuthAngle;
	}
	
	public boolean isStep() {
		return this.isStep;
	}
	
	public double weAcceration() {
		return this.weAcceration;
	}
	
	public double nsAcceleration() {
		return this.nsAcceleration;
	}
	
	public double udAcceleration() {
		return this.nsAcceleration;
	}
}
