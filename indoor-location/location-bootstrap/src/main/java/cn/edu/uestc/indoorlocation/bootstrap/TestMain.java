package cn.edu.uestc.indoorlocation.bootstrap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.edu.uestc.indoorlocation.communication.server.DefaultNettyServerAcceptor;

/**
 * 测试类
 * @author vincent
 *
 */
public class TestMain {

	private static Logger LOGGER = LoggerFactory.getLogger(TestMain.class);
	
	public static void main(String[] args) throws InterruptedException {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-context.xml");
		DefaultNettyServerAcceptor server  = (DefaultNettyServerAcceptor) ctx.getBean("defaultServer");
		server.start();  //start
	}
}